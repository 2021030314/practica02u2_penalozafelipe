/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author felip
 */
public class dbProductos extends dbManejador implements dbPersistencia{

    @Override
    public void insertar(Object objeto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;
        String consulta = "insert into " + "productos(codigo, nombre, fecha, precio, status) values (?, ?, ?, ?, ?)";
        String consulta2 = "select idProducto from productos";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPrecio());
                this.sqlConsulta.setInt(5, pro.getStatus());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public void actualizar(Object objeto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;
        String consulta = "update productos set nombre = ?, fecha = ?, precio = ? where codigo = ?";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta

                this.sqlConsulta.setString(1, pro.getNombre());
                this.sqlConsulta.setString(2, pro.getFecha());
                this.sqlConsulta.setFloat(3, pro.getPrecio());
                this.sqlConsulta.setString(4, pro.getCodigo());
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public void habilitar(Object objeto, String codigo) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;
        String consulta = "update productos set status = 1 where codigo = ?";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                
                this.sqlConsulta.setString(1, codigo);
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public void deshabilitar(Object objeto, String codigo) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objeto;
        String consulta = "update productos set status = 0 where codigo = ?";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                
                this.sqlConsulta.setString(1, codigo);
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public boolean isExiste(String codigo) throws Exception {
        boolean confirm = false;
        if(this.conectar()){
            String consulta = "Select * from productos where codigo = ?";
            this.sqlConsulta = conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                confirm = true;
            }
        }
        this.desconectar();
        return confirm;
    }

    @Override
    public ArrayList listar(String criterio) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;
        if(this.conectar()){
            //String consulta = "Select * from productos order by codigo";
            String consulta = "Select * from productos where status = 0 order by codigo";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            
            while(this.registros.next()){
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
                lista.add(pro);
            }
        }
        this.desconectar();
        return lista;
    }

    @Override
    public Object buscar(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();
        if(this.conectar()){
            String consulta = "Select * from productos where codigo = ? and status = 1";
            
            this.sqlConsulta = conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            
            if(this.registros.next()){
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
            }
        }
        this.desconectar();
        return pro;
    }

    @Override
    public ArrayList listar2() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;
        if(this.conectar()){
            //String consulta = "Select * from productos order by codigo";
            String consulta = "Select * from productos where status = 1 order by codigo";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            
            while(this.registros.next()){
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
                lista.add(pro);
            }
        }
        this.desconectar();
        return lista;
    }

    @Override
    public Object buscar2(String codigo) throws Exception {
        Productos pro = new Productos();
        if(this.conectar()){
            String consulta = "Select * from productos where codigo = ? and status = 0";
            
            this.sqlConsulta = conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            
            if(this.registros.next()){
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
                pro.setNombre(this.registros.getString("nombre"));
            }
        }
        this.desconectar();
        return pro;
    }
    
}
