/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.util.ArrayList;

/**
 *
 * @author felip
 */
public interface dbPersistencia {
    public void insertar(Object objeto) throws Exception;
    public void actualizar(Object objeto) throws Exception;
    public void habilitar(Object objeto, String codigo) throws Exception;
    public void deshabilitar(Object objeto, String codigo) throws Exception;
    
    public boolean isExiste(String codigo) throws Exception;
    public ArrayList listar(String criterio) throws Exception;
    public ArrayList listar() throws Exception;
    public ArrayList listar2() throws Exception;
    
    public Object buscar(int id) throws Exception;
    public Object buscar(String codigo) throws Exception;
    public Object buscar2(String codigo) throws Exception;
    
}
