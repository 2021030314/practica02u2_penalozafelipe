/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Modelos.Productos;
import Modelos.dbProductos;
import Vistas.dlgProductos;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import java.util.Date;

public class Controlador implements ActionListener {
    private Productos pro;
    private dbProductos db;
    private dlgProductos vista;
    private int option = 0;
    
    public Controlador(Productos pro, dlgProductos vista, dbProductos db){
        this.db = db;
        this.pro = pro;
        this.vista = vista;
        
        vista.btnLimpiar.addActionListener(this);
        vista.btnGA.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnBuscar2.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.spinnerStatus.addChangeListener(null);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
    }
    
    private void cargarDatos(){
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Productos> lista2 = new ArrayList<>();
        try{
            lista2 = db.listar2();
        }
        catch(Exception ex){
            
        }
        modelo.addColumn("Id");
        modelo.addColumn("Codigo");
        modelo.addColumn("Nombre");
        modelo.addColumn("Fecha");
        modelo.addColumn("Precio");
        modelo.addColumn("Status");
        for(Productos producto: lista2){
            modelo.addRow(new Object[]{producto.getIdProducto(), producto.getCodigo(), producto.getNombre(), producto.getFecha(), producto.getPrecio(), producto.getStatus()});
        }
        vista.tableDatos.setModel(modelo);
    }
    
    private void cargarDatosDes(){
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Productos> lista2 = new ArrayList<>();
        try{
            lista2 = db.listar();
        }
        catch(Exception ex){
            
        }
        modelo.addColumn("Id");
        modelo.addColumn("Codigo");
        modelo.addColumn("Nombre");
        modelo.addColumn("Fecha");
        modelo.addColumn("Precio");
        modelo.addColumn("Status");
        for(Productos producto: lista2){
            modelo.addRow(new Object[]{producto.getIdProducto(), producto.getCodigo(), producto.getNombre(), producto.getFecha(), producto.getPrecio(), producto.getStatus()});
        }
        vista.tableDatos2.setModel(modelo);
    }
    
    private void iniciarV(){
        vista.setTitle("-¡- PRODUCTOS -¡-");
        vista.setSize(730, 550);
        vista.setVisible(true);
    }
    
    private void limpiar(){
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.jdFecha.setDate(null);
        vista.spinnerStatus.setValue(1);
        vista.txtCodigo.setEnabled(true);
        option = 0;
        vista.lblState.setText("MODO DE AGREGACIÓN");
        //vista.txtStatus.setText("");
    }

    public static void main(String[] args) {
        Productos pro = new Productos();
        dbProductos db = new dbProductos();
        dlgProductos vista = new dlgProductos(new JFrame(), true);
        Controlador cr = new Controlador(pro, vista, db);
        try{
            cr.cargarDatos();
            cr.cargarDatosDes();
        }
        catch(Exception ex){
            System.out.println("Surgió un error " + ex.getMessage());
        }
        cr.iniciarV();
        
        /*pro.setCodigo("0011011");
        pro.setNombre("Harina de Trigo");
        pro.setPrecio(33.50f);
        pro.setFecha("2023-06-23");
        pro.setStatus(0);
        
        try{
            db.insertar(pro);
            System.out.println("Se agregó con éxito");
            db.habilitar(pro);
            System.out.println("Se habilitó");
            Productos res = new Productos();
            res = (Productos) db.buscar("0011011");
            System.out.println("Nombre: " + res.getNombre() + "Precio: " + res.getPrecio() + "Status: " + res.getStatus());
        }
        catch(Exception e){
            System.err.println("Surgió un error" + e.getMessage());
        }*/
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnNuevo){
            vista.txtCodigo.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.btnBuscar.setEnabled(true);
            vista.btnGA.setEnabled(true);
            vista.btnDeshabilitar.setEnabled(true);
            //vista.btnHabilitar.setEnabled(true);
            vista.spinnerStatus.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.jdFecha.setEnabled(true);
            vista.lblState.setText("MODO DE AGREGACIÓN");
        }
        
        if(e.getSource() == vista.btnCancelar){
            vista.txtCodigo.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.btnBuscar.setEnabled(false);
            vista.btnGA.setEnabled(false);
            vista.btnDeshabilitar.setEnabled(false);
            //vista.btnHabilitar.setEnabled(false);
            vista.spinnerStatus.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.jdFecha.setEnabled(false);
            vista.lblState.setText("");
            vista.btnLimpiar.doClick();
        }
        
        if(e.getSource() == vista.btnLimpiar){
            option = 0;
            limpiar();
        }
        
        if(e.getSource() == vista.btnGA){
            DefaultTableModel modelo = (DefaultTableModel)vista.tableDatos.getModel();
            Object [] fila = new Object [6];
            try{
                if(option == 1){
                   int choice = JOptionPane.showConfirmDialog(vista, "¿ACTUALIZAR?");
                   if(choice == 0){
                       pro.setCodigo(vista.txtCodigo.getText());
                       pro.setNombre(vista.txtNombre.getText());
                       pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                       String date = sdf.format(vista.jdFecha.getDate());
                       pro.setFecha(date);
                       pro.setStatus(0); 
                       db.actualizar(pro);
                   }
                   option = 0;
                   limpiar();
                   cargarDatos();
                   cargarDatosDes();
                   vista.txtCodigo.setEnabled(true);
                   vista.lblState.setText("MODO DE AGREGACIÓN");
                   return;
                }
                if(!vista.txtCodigo.getText().equals("") && db.isExiste(vista.txtCodigo.getText()) == false){
                    JOptionPane.showMessageDialog(vista, "AGREGADO");
                    pro.setCodigo(vista.txtCodigo.getText());
                    pro.setNombre(vista.txtNombre.getText());                   
                    pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));                   
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String date = sdf.format(vista.jdFecha.getDate());
                    pro.setFecha(date);                   
                    pro.setStatus((int)vista.spinnerStatus.getValue()); 
                    db.insertar(pro);  
                    cargarDatos();
                    cargarDatosDes();
                }
                else{
                    JOptionPane.showMessageDialog(vista, "VALOR INVÁLIDO");
                }
            }
            catch(Exception ex){
                System.out.println("Surgió un error" + ex.getMessage());
            }
        }
        
        if(e.getSource() == vista.btnBuscar){
            try{
                Productos res = new Productos();
                if(db.isExiste(vista.txtCodigo.getText()) == true){
                    res = (Productos)db.buscar(vista.txtCodigo.getText());
                    vista.txtCodigo.setEnabled(false);
                    vista.txtNombre.setText(res.getNombre());
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date fecha = sdf.parse(res.getFecha());
                    vista.jdFecha.setDate(fecha);
                    vista.spinnerStatus.setValue(res.getStatus());
                    vista.txtPrecio.setText(String.valueOf(res.getPrecio()));
                    this.option = 1;
                    vista.lblState.setText("MODO DE ACTUALIZACIÓN");
                    JOptionPane.showMessageDialog(vista, "ENCONTRADO");
                }
                else{
                    JOptionPane.showMessageDialog(vista, "CÓDIGO INEXISTENTE");
                }
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(vista, "CÓDIGO INEXISTENTE");
                System.out.println("Surgió un error" + ex.getMessage());
                vista.btnLimpiar.doClick();
            }
        }
        
        if(e.getSource() == vista.btnDeshabilitar){
            try{
                db.deshabilitar(pro, vista.txtCodigo.getText());
                cargarDatos();
                cargarDatosDes();
            }
            catch(Exception ex){
                System.out.println("Surgió un error" + ex.getMessage());
            }
            
        }
        
        if(e.getSource() == vista.btnHabilitar){
            try{
                db.habilitar(pro, vista.txtCodigo2.getText());
                cargarDatos();
                cargarDatosDes();
            }
            catch(Exception ex){
                System.out.println("Surgió un error" + ex.getMessage());
            }
            
        }
        
        if(e.getSource() == vista.btnBuscar2){
            try{
                Productos res = new Productos();
                if(db.isExiste(vista.txtCodigo2.getText()) == true){
                    res = (Productos)db.buscar2(vista.txtCodigo2.getText());
                    //vista.txtCodigo.setEnabled(false);
                    vista.txtNombre2.setText(res.getNombre());
                    JOptionPane.showMessageDialog(vista, "ENCONTRADO");
                    //this.option = 1;
                }
                else{
                    JOptionPane.showMessageDialog(vista, "CÓDIGO INEXISTENTE");
                }
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(vista, "CÓDIGO INEXISTENTE");
                System.out.println("Surgió un error" + ex.getMessage());
                vista.btnLimpiar.doClick();
            }
        }
    }
}
